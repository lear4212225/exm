package main

import (
	"awesomeProject/rabbit"
	"awesomeProject/serverproto"
	"context"
	"log"
	"time"
)

func main() {
	q := rabbit.NewRabbitMQ()
	client(q)

}

func client(q rabbit.Queuer) {
	order := serverproto.OrderRequest{
		Id:          55,
		ProductName: "hello",
		Quantity:    10,
		TotalPrice:  5,
		Client:      24,
	}
	err := q.SendOrder(context.Background(), order.String())
	if err != nil {
		log.Fatal(err)
	}
	time.Sleep(1 * time.Second)

	del, err := q.GetStatus()
	var msgString string
	for msg := range del {
		log.Println(msg.Body)
		msgString += string(msg.Body)
		if err := msg.Ack(false); err != nil {
			log.Printf("Error acknowledging message : %s", err)
		} else {
			log.Printf("Acknowledged message")
		}
	}

	var yo serverproto.OrderStatusResponse
	if msgString == "true" {
		yo = serverproto.OrderStatusResponse{Success: true}
		log.Println("success")
	} else {
		yo = serverproto.OrderStatusResponse{Success: false}
		log.Println("un success")
	}
	log.Println(yo)
	time.Sleep(49 * time.Second)

}
