package main

import (
	"awesomeProject/rabbit"
	"awesomeProject/serverproto"
	"context"
	"log"
	"strconv"
	"strings"
	"time"
)

func main() {
	q := rabbit.NewRabbitMQ()
	server(q)

}

func server(q rabbit.Queuer) {
	del, err := q.GetOrder()
	if err != nil {
		log.Fatal(err)
	}

	var msgString string
	for msg := range del {
		msgString += string(msg.Body)
		if err := msg.Ack(false); err != nil {
			log.Printf("Error acknowledging message : %s", err)
		} else {
			log.Printf("Acknowledged message")
		}
	}

	msgSplit := strings.Split(msgString, " ")

	id, err := strconv.Atoi(strings.Split(msgSplit[0], ":")[1])
	if err != nil {
		_ = q.SendStatus(context.Background(), "false")
		log.Fatal(err)
	}
	quantity, err := strconv.Atoi(strings.Split(msgSplit[2], ":")[1])
	if err != nil {
		_ = q.SendStatus(context.Background(), "false")
		log.Fatal(err)
	}
	totalPrice, err := strconv.ParseFloat(strings.Split(msgSplit[3], ":")[1], 32)
	if err != nil {
		_ = q.SendStatus(context.Background(), "false")
		log.Fatal(err)

	}
	client, err := strconv.Atoi(strings.Split(msgSplit[4], ":")[1])
	if err != nil {
		_ = q.SendStatus(context.Background(), "false")
		log.Fatal(err)
	}
	order := serverproto.OrderRequest{
		Id:          int32(id),
		ProductName: strings.Trim(strings.Split(msgSplit[1], ":")[1], "\""),
		Quantity:    int32(quantity),
		TotalPrice:  float32(totalPrice),
		Client:      int32(client),
	}
	log.Println(order.String())

	err = q.SendStatus(context.Background(), "true")
	time.Sleep(49 * time.Second)

}
