package rabbit

import (
	"context"
	"fmt"
	"log"

	amqp "github.com/rabbitmq/amqp091-go"
)

type Queuer interface {
	GetOrder() (<-chan amqp.Delivery, error)
	SendOrder(ctx context.Context, mes string) error
	SendStatus(ctx context.Context, mes string) error
	GetStatus() (<-chan amqp.Delivery, error)
}

type RabbitMQ struct {
	ch          *amqp.Channel
	order       *amqp.Queue
	orderStatus *amqp.Queue
}

func NewRabbitMQ() Queuer {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", "guest", "guest", "localhost", "5672"))
	if err != nil {
		log.Fatal(err)
	}
	log.Println("connected to RabbitMQ")
	ch, err := conn.Channel()
	if err != nil {
		log.Fatal(err)
	}

	order, err := ch.QueueDeclare(
		"order", // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	if err != nil {
		log.Fatal(err)
	}

	orderStatus, err := ch.QueueDeclare(
		"orderStatus", // name
		false,         // durable
		false,         // delete when unused
		false,         // exclusive
		false,         // no-wait
		nil,           // arguments
	)
	if err != nil {
		log.Fatal(err)
	}

	return &RabbitMQ{
		ch:          ch,
		order:       &order,
		orderStatus: &orderStatus,
	}
}

func (p *RabbitMQ) GetOrder() (<-chan amqp.Delivery, error) {
	msg, err := p.ch.Consume(
		p.order.Name, // queue
		"",           // consumer
		true,         // auto-ack
		false,        // exclusive
		false,        // no-local
		false,        // no-wait
		nil,          // args
	)
	return msg, err
}

func (p *RabbitMQ) SendOrder(ctx context.Context, mes string) error {
	err := p.ch.PublishWithContext(ctx,
		"",           // exchange
		p.order.Name, // routing key
		false,        // mandatory
		false,        // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(mes),
		})
	return err
}

func (p *RabbitMQ) SendStatus(ctx context.Context, mes string) error {
	err := p.ch.PublishWithContext(ctx,
		"",                 // exchange
		p.orderStatus.Name, // routing key
		false,              // mandatory
		false,              // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(mes),
		})
	return err
}

func (p *RabbitMQ) GetStatus() (<-chan amqp.Delivery, error) {
	msg, err := p.ch.Consume(
		p.orderStatus.Name, // queue
		"",                 // consumer
		true,               // auto-ack
		false,              // exclusive
		false,              // no-local
		false,              // no-wait
		nil,                // args
	)
	return msg, err
}
